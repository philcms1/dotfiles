alias l="exa -la --header --git"
alias lt="exa --tree --long --level=2"

ZSH_THEME="agnoster"
plugins=(git)

eval "$(starship init zsh)"